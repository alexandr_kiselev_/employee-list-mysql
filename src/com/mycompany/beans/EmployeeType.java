package com.mycompany.beans;

/**
 * Type of employee.
 */
public enum EmployeeType {
  WORKER("Worker"),
  MANAGER("Manager"),
  SPECIALIST("Specialist");

  private final String employeeType;

  EmployeeType(final String employeeType) {
    this.employeeType = employeeType;
  }

  public String getEmployeeType() {
    return employeeType;
  }
}
