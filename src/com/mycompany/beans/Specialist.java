package com.mycompany.beans;

import java.util.Date;
import java.util.Objects;

/**
 * Class contain information about specialist.
 */
public class Specialist extends Employee {
  /**
   * Specialist description, position.
   */
  private String description;

  public Specialist(final int id, final String lastName, final String firstName, final String patronymic,
                    final Date dateEmployment, final Date dateBirth, final String description) {
    super(id, lastName, firstName, patronymic, EmployeeType.SPECIALIST, dateEmployment, dateBirth);
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return "Specialist{" +
      "id='" + id + '\'' +
      ", lastName='" + lastName + '\'' +
      ", firstName='" + firstName + '\'' +
      ", patronymic='" + patronymic + '\'' +
      ", employeeType='" + employeeType.getEmployeeType() + '\'' +
      ", dateEmployment='" + dateEmployment + '\'' +
      ", dateBirth='" + dateBirth + '\'' +
      ", description='" + description + '\'' +
      '}';
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Specialist)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Specialist that = (Specialist) o;
    return Objects.equals(getDescription(), that.getDescription());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), getDescription());
  }
}
