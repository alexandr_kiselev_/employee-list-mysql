package com.mycompany.util;

import com.mycompany.beans.EmployeeType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Class for get information from console.
 */
public class Console {
  /**
   * Simple text scanner.
   */
  private Scanner scan;

  public Console() {
    scan = new Scanner(System.in);
  }

  /**
   * Get string from console with printing label before.
   *
   * @param textLabel - printing label.
   * @return input string.
   */
  public String getStringFromConsole(final String textLabel) {
    String result;
    do {
      System.out.print(textLabel);
      scan.hasNextLine();
      result = scan.nextLine();
    } while (result.length() == 0);

    return result;
  }

  /**
   * Get date from console with printing label before.
   *
   * @param textLabel - printing label.
   * @return date in format dd.MM.yyyy.
   */
  public Date getDateFromConsole(final String textLabel) {
    Date result = null;
    DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    do {
      System.out.print(textLabel);
      scan.hasNextLine();
      try {
        result = format.parse(scan.nextLine());
      } catch (ParseException e) {
        System.out.print("You input wrong date!");
      }
    } while (result == null);

    return result;
  }

  /**
   * Get index from console with printing label before.
   * Index mast be from 0 to maxValue
   *
   * @param textLabel - printing label.
   * @param maxValue  - maximal value for index.
   * @return index.
   */
  public int getIndexFromConsole(final String textLabel, final int maxValue) {
    int result = -1;
    do {
      System.out.print(textLabel);
      if (!scan.hasNextInt()) {
        scan.nextLine();
        continue;
      }
      result = scan.nextInt();
      if (result < 0 || result > maxValue) {
        System.out.println("Number mast be from 0 to " + maxValue);
      }
    } while (result < 0 || result > maxValue);

    return result;
  }

  /**
   * Get type of employee.
   * Index mast be from 0 to maxValue
   *
   * @return index.
   */
  public EmployeeType getIndexEmployeeType() {
    String choice;
    do {
      System.out.println("1. Worker");
      System.out.println("2. Manger");
      System.out.println("3. Specialist");
      System.out.println("0. Return back");
      System.out.print("Select employee type: ");

      scan.hasNextLine();
      choice = scan.nextLine();

      switch (choice) {
        case "1":
          return EmployeeType.WORKER;
        case "2":
          return EmployeeType.MANAGER;
        case "3":
          return EmployeeType.SPECIALIST;
        default:
          break;
      }
    } while (!choice.equals("0") && !choice.equals("1") && !choice.equals("2") && !choice.equals("3"));

    return null;
  }
}
