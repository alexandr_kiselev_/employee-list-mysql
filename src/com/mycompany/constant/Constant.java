package com.mycompany.constant;

/**
 * Class contains constants.
 */
public final class Constant {
  private Constant() {
  }

  public static final String EMPLOYEE_LIST_EMPTY = "List of employees is empty.";
}