package com.mycompany.controller;

import com.mycompany.beans.Employee;
import com.mycompany.beans.EmployeeType;
import com.mycompany.beans.Manager;
import com.mycompany.beans.Specialist;
import com.mycompany.beans.Worker;
import com.mycompany.constant.Constant;
import com.mycompany.model.EmployeeDAO;
import com.mycompany.util.Console;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Class contain all employees of company, and  methods for work with employee list.
 */
public class EmployeeController {
  /**
   * List of all employees.
   */
  private List<Employee> employeeList;
  private EmployeeDAO employeeDAO;
  private Console console;

  EmployeeController() {
    employeeList = new ArrayList<>();
    employeeDAO = new EmployeeDAO();
    console = new Console();

    loadEmployeeListFromDB();
  }

  /**
   * Add employee by type, get data from console.
   *
   * @param employeeType - type of employee.
   * @return true - success, false - fail.
   */
  boolean addEmployeeByType(final EmployeeType employeeType) {

    String lastName, firstName, patronymic, description = "";
    Date dateEmployment, dateBirth;

    lastName = console.getStringFromConsole("Input last name: ");
    firstName = console.getStringFromConsole("Input first name: ");
    patronymic = console.getStringFromConsole("Input patronymic: ");

    dateEmployment = console.getDateFromConsole("Input date of employment (dd.MM.yyyy): ");
    dateBirth = console.getDateFromConsole("Input date of birth (dd.MM.yyyy): ");

    if (employeeType == EmployeeType.SPECIALIST) {
      description = console.getStringFromConsole("Input specialist description: ");
    }

    Employee newEmployee = createEmployeeByType(0, lastName, firstName, patronymic, dateEmployment, dateBirth,
        employeeType, description);
    return employeeList.add(newEmployee);
  }

  /**
   * Print list of employees, and get index for remove employee, remove employee.
   *
   * @return true - success, false - fail.
   */
  boolean removeEmployee() {

    if (employeeList.isEmpty()) {
      System.out.println(Constant.EMPLOYEE_LIST_EMPTY);
      return false;
    }

    int id;

    printEmployeeList();
    id = console.getIndexFromConsole("Input number of employee for remove from 1 to " + employeeList.size()
        + " (0 - for cancel): ", employeeList.size());
    if (id > 0) {
      employeeDAO.removeEmployee(id - 1);
      employeeList.remove(id - 1);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Loading list of employee from DB.
   *
   * @return true - success, false - fail.
   */
  private boolean loadEmployeeListFromDB() {
    employeeList = employeeDAO.getAllEmployee();
    linkWorkersToManagers();

    return true;
  }

  /**
   * Link worker to manager in all list of employee.
   *
   * @return true - success, false - fail.
   */
  private boolean linkWorkersToManagers() {

    for (int i = 0; i < employeeList.size(); i++) {
      if (employeeList.get(i).getEmployeeType().equals(EmployeeType.WORKER) &&
          ((Worker) employeeList.get(i)).getManagerId() > 0) {
        for (int j = i; j < employeeList.size(); j++) {
          if (employeeList.get(j).getId() == ((Worker) employeeList.get(i)).getManagerId() &&
              employeeList.get(j).getEmployeeType().equals(EmployeeType.MANAGER)) {
            ((Manager) employeeList.get(j)).addSubordinateEmployee(((Worker) employeeList.get(i)));
          }
        }
      }
    }
    return true;
  }

  /**
   * Link one worker to one manager.
   *
   * @return true - success, false - fail.
   */
  boolean setManagerForEmployee() {
    List<Integer> managerList, workerList;
    int managerId, workerId;

    if (employeeList.isEmpty()) {
      System.out.println(Constant.EMPLOYEE_LIST_EMPTY);
      return false;
    }


    managerList = printEmployeeByType(Manager.class);
    if (managerList.isEmpty()) {
      System.out.println("No managers. Please, add manager before");
      return false;
    }

    managerId = console.getIndexFromConsole("Input number of manager for link with worker from 1 to " + managerList.size()
        + " (0 - for cancel): ", managerList.size());

    if (managerId == 0) {
      System.out.println("Operation canceled.");
      return false;
    }

    workerList = printEmployeeByType(Worker.class);
    if (workerList.isEmpty()) {
      System.out.println("No workers. Please, add worker before");
      return false;
    }

    workerId = console.getIndexFromConsole("Input number of worker for link with manager from 1 to " + workerList.size()
        + " (0 - for cancel): ", workerList.size());

    if (workerId == 0) {
      System.out.println("Operation canceled.");
      return false;
    }

    if (employeeList.get(managerList.get(managerId - 1)).getId() !=
        ((Worker) employeeList.get(workerList.get(workerId - 1))).getManagerId()) {
      ((Manager) employeeList.get(managerList.get(managerId - 1))).
          addSubordinateEmployee((Worker) employeeList.get(workerList.get(workerId - 1)));
      employeeDAO.updateEmployee(employeeList.get(workerList.get(workerId - 1)));
      return true;
    }

    return false;
  }

  /**
   * Prints employees only current type.
   *
   * @param classType - class type employee.
   * @return list of id for printed employees.
   */
  private List<Integer> printEmployeeByType(final Class<?> classType) {
    ArrayList<Integer> idEmployee = new ArrayList<>();

    for (int i=0;i<employeeList.size();i++) {
      if (classType.isInstance(employeeList.get(i))) {
        idEmployee.add(i);
        System.out.println((idEmployee.size()) + ". " + employeeList.get(i));
      }
    }

    return idEmployee;
  }

  /**
   * Print list of employees, and get index for change type employee, get index new typeof employee,
   * change type of employee.
   *
   * @return true - success, false - fail.
   */
  boolean changeTypeEmployee() {
    if (employeeList.isEmpty()) {
      System.out.println(Constant.EMPLOYEE_LIST_EMPTY);
      return false;
    }

    int id;

    printEmployeeList();
    id = console.getIndexFromConsole("Input number of employee for change type from 1 to " + employeeList.size()
        + " (0 - for cancel): ", employeeList.size());
    if (id > 0) {
      EmployeeType newEmployeeType = console.getIndexEmployeeType();
      if (newEmployeeType != null) {
        employeeList.set(id - 1, changeTypeEmployeeById(employeeList.get(id - 1), newEmployeeType));
        employeeDAO.updateEmployee(employeeList.get(id - 1));
        return true;
      } else {
        return false;
      }

    } else {
      return false;
    }
  }

  /**
   * Changing type of employee.
   *
   * @return employee.
   */
  private Employee changeTypeEmployeeById(final Employee employee, final EmployeeType newEmployeeType) {
    Employee newEmployee;
    String description = "";

    if (employee.getEmployeeType() != newEmployeeType) {
      if (employee.getEmployeeType() == EmployeeType.MANAGER) {
        for (Employee subordinationEmployee :
            ((Manager) employee).getWorkerList()) {
          if (subordinationEmployee instanceof Worker)
            ((Worker) subordinationEmployee).setManagerId(0);
        }
      }

      if (employee.getEmployeeType() == EmployeeType.WORKER) {
        for (Employee emp :
            employeeList) {
          if (emp instanceof Manager)
            ((Manager) emp).getWorkerList().remove(employee);
        }
      }

      if (newEmployeeType == EmployeeType.SPECIALIST) {
        description = console.getStringFromConsole("Input specialist description: ");
      }

      newEmployee = createEmployeeByType( 0, employee.getLastName(),
          employee.getFirstName(), employee.getPatronymic(), employee.getDateEmployment(), employee.getDateBirth(),
          newEmployeeType, description);

      newEmployee.setId(employee.getId());

      return newEmployee;
    }
    return employee;
  }

  /**
   * Create employee by type.
   *
   * @return employee.
   */
  private Employee createEmployeeByType(final int managerId, final String lastName,
                                        final String firstName, final String patronymic,
                                        final Date dateEmployment, final Date dateBirth, final EmployeeType employeeType,
                                        final String description) {
    Employee newEmployee = null;
    switch (employeeType) {
      case WORKER:
        newEmployee = new Worker(0, managerId, lastName, firstName, patronymic, dateEmployment, dateBirth);
        break;
      case MANAGER:
        newEmployee = new Manager(0, lastName, firstName, patronymic, dateEmployment, dateBirth);
        break;
      case SPECIALIST:
        newEmployee = new Specialist(0, lastName, firstName, patronymic, dateEmployment, dateBirth, description);
        break;
      default:
        break;
    }

    employeeDAO.addNewEmployee(newEmployee);
    return newEmployee;
  }

  /**
   * Sort list of employee by last name.
   */
  void sortEmployeeListByLastName() {
    employeeList.sort(Comparator.comparing(Employee::getLastName));
  }

  /**
   * Sort list of employee by date of employment.
   */
  void sortEmployeeListByDateEmployment() {
    employeeList.sort(Comparator.comparing(Employee::getDateEmployment));
  }

  public List<Employee> getEmployeeList() {
    return employeeList;
  }

  public void setEmployeeList(final List<Employee> employeeSet) {
    if (employeeSet != null)
      this.employeeList = employeeSet;
  }

  /**
   * Print list of employee.
   */
  void printEmployeeList() {
    if (employeeList.isEmpty()) {
      System.out.println(Constant.EMPLOYEE_LIST_EMPTY);
    } else {
      System.out.println(this);
    }
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (employeeList == null) {
      return "EmployeeController{employeeList=null}";
    } else {
      int i=0;
      stringBuilder.append("EmployeeController{employeeList=\n");
      for (Employee employee : employeeList
          ) {
        stringBuilder.append(++i + ". ").append("{").append(employee).append("}\n");
      }
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
}
