package com.mycompany.model;

import com.mycompany.beans.Employee;
import com.mycompany.beans.Manager;
import com.mycompany.beans.Specialist;
import com.mycompany.beans.Worker;
import com.mycompany.util.ConnectionFactory;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for work with DB for employee.
 */
public class EmployeeDAO {

  private Connection connection;

  public EmployeeDAO() {
    this.connection = new ConnectionFactory().getConnection();
    createTable();
  }

  /**
   * Create table in DB.
   */
  private void createTable() {
    String query = "CREATE TABLE IF NOT EXISTS `employee` (" +
        "`id` INT AUTO_INCREMENT," +
        "`managerId` INT," +
        "`lastName` VARCHAR(64) NOT NULL," +
        "`firstName` VARCHAR(64) NOT NULL," +
        "`patronymic` VARCHAR(64) NOT NULL," +
        "`employeeType` ENUM('WORKER','MANAGER','SPECIALIST') NOT NULL," +
        "`dateEmployment` DATE NOT NULL," +
        "`dateBirth` DATE NOT NULL," +
        "`description` VARCHAR(128)," +
        "PRIMARY KEY (`id`));";

    Statement statement = null;
    try {
      statement = connection.createStatement();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    try {
      statement.execute(query);
    } catch (SQLException e) {
      e.printStackTrace();
    }

//    query = "INSERT INTO `employee` (`id`, `managerId`, `lastName`, `firstName`, `patronymic`, `employeeType`, `dateEmployment`, `dateBirth`, `description`) VALUES" +
//        "(1, 2, 'Ivanov', 'Ivan', 'Ivanovich', 'WORKER', '2018-06-20', '2000-05-11', NULL), " +
//        "(2, 0, 'Sidorov', 'Sidor', 'Sidorovich', 'MANAGER', '2018-07-21', '1999-12-10', NULL), " +
//        "(3, 0, 'Petrov', 'Petr', 'Petrovich', 'SPECIALIST', '2018-09-13', '1998-07-09', 'Director of company');";

    try {
      statement.execute(query);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Add new employee to DB.
   *
   * @param employee
   */
  public void addNewEmployee(final Employee employee) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


    String sql = "INSERT INTO employee (managerId, lastName, firstName, patronymic, "
        + "employeeType, dateEmployment, dateBirth, description) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    try {
      PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      if (employee instanceof Worker) {
        preparedStatement.setInt(1, ((Worker) employee).getManagerId());
      } else {
        preparedStatement.setInt(1, 0);
      }

      preparedStatement.setString(2, employee.getLastName());
      preparedStatement.setString(3, employee.getFirstName());
      preparedStatement.setString(4, employee.getPatronymic());
      preparedStatement.setString(5, employee.getEmployeeType().name());
      preparedStatement.setDate(6, Date.valueOf(formatter.format(employee.getDateEmployment())));
      preparedStatement.setDate(7, Date.valueOf(formatter.format( employee.getDateBirth())));
      if (employee instanceof Specialist) {
        preparedStatement.setString(8, ((Specialist) employee).getDescription());
      } else {
        preparedStatement.setString(8, "");
      }

      preparedStatement.execute();

      ResultSet resultSet = preparedStatement.getGeneratedKeys();
      if (resultSet.next()) {
        employee.setId(resultSet.getInt(1));
      }

      resultSet.close();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Update record employee in DB.
   *
   * @param employee
   */
  public void updateEmployee(final Employee employee) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "UPDATE employee SET managerId=?, lastName=?,  firstName=?, patronymic=?, " +
        "employeeType=?, dateEmployment=?, dateBirth=?, description=? WHERE id=?";

    try {
      PreparedStatement preparedStatement = connection.prepareStatement(sql);

      if (employee instanceof Worker) {
        preparedStatement.setInt(1, ((Worker) employee).getManagerId());
      } else {
        preparedStatement.setInt(1, 0);
      }

      preparedStatement.setString(2, employee.getLastName());
      preparedStatement.setString(3, employee.getFirstName());
      preparedStatement.setString(4, employee.getPatronymic());
      preparedStatement.setString(5, employee.getEmployeeType().name());
      preparedStatement.setDate(6, Date.valueOf(formatter.format(employee.getDateEmployment())));
      preparedStatement.setDate(7, Date.valueOf(formatter.format( employee.getDateBirth())));
      if (employee instanceof Specialist) {
        preparedStatement.setString(8, ((Specialist) employee).getDescription());
      } else {
        preparedStatement.setString(8, "");
      }
      preparedStatement.setInt(9, employee.getId());

      preparedStatement.execute();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Remove employee from BD.
   *
   * @param employee_id
   */
  public void removeEmployee(final int employee_id) {
    String sql = "DELETE FROM employee WHERE id=?";

    try {
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      preparedStatement.setInt(1, employee_id);
      preparedStatement.execute();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public List<Employee> getAllEmployee() {

    List<Employee> employeeList = new ArrayList<>();

    String sql = "SELECT * FROM employee ORDER BY lastName";

    try {
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      ResultSet resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        Employee newEmployee = null;
        switch (resultSet.getString("employeeType")) {
          case "WORKER":
            newEmployee = new Worker(resultSet.getInt("id"), resultSet.getInt("managerId"),
                resultSet.getString("firstName"), resultSet.getString("lastName"),
                resultSet.getString("patronymic"), resultSet.getDate("dateEmployment"),
                resultSet.getDate("dateBirth"));
            break;
          case "MANAGER":
            newEmployee = new Manager(resultSet.getInt("id"),
                resultSet.getString("firstName"), resultSet.getString("lastName"),
                resultSet.getString("patronymic"), resultSet.getDate("dateEmployment"),
                resultSet.getDate("dateBirth"));
            break;
          case "SPECIALIST":
            newEmployee = new Specialist(resultSet.getInt("id"),
                resultSet.getString("firstName"), resultSet.getString("lastName"),
                resultSet.getString("patronymic"), resultSet.getDate("dateEmployment"),
                resultSet.getDate("dateBirth"), resultSet.getString("description"));
            break;
          default:
            break;
        }
        employeeList.add(newEmployee);
      }

      resultSet.close();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return employeeList;
  }

}
