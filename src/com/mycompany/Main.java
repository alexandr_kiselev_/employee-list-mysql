package com.mycompany;

import com.mycompany.controller.MenuController;
import com.mycompany.util.ConnectionFactory;

public class Main {

  public static void main(String[] args) {

    MenuController menuController = new MenuController();
    menuController.printMenu();
  }
}
